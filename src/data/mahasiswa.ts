export interface Mahasiswa {
    user_id: string;
    email: string;
    name: string;
    nim: string;
    prodi: string;
    angkatan: string;
    type: string;
}