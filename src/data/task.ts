export class Task{
    id: number;
    lecturer_id: string;
    type: number;
    description: string; 
    sort: number;
    status_done: number;
    weight: number;
}