import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthService } from "../../services/authService";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Mahasiswa } from "../../data/mahasiswa";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import swal from 'sweetalert2';
import { Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: "page-edit-profile",
  templateUrl: "edit-profile.html"
})
export class EditProfilePage {
  editProfileForm: FormGroup;
  user: Mahasiswa;
  imageURI:any;
  imageFileName:any;
  imgUrl:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authService: AuthService,
    private camera: Camera,
    private transfer: FileTransfer,
    public events: Events
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad EditProfilePage");
  }

  ngOnInit() {
    let emailText = this.authService.email;
    let nim = this.authService.nim;
    let prodi = this.authService.prodi;
    let angkatan = this.authService.angkatan;
    let name = this.authService.name;
    let user_id = this.authService.user_id;
    this.user = {
      user_id: user_id,
      email: emailText,
      nim: nim,
      prodi: prodi,
      angkatan: angkatan,
      name: name,
      type: +this.authService.position == 0 ? "Lecturer" : "Student"
    };
    console.log(this.user);
    this.initializeForm();
  }

  private initializeForm() {
    this.editProfileForm = new FormGroup({
      // emailText: new FormControl(null, Validators.required),
      name: new FormControl(null, Validators.required),
      nim: new FormControl(null),
      prodi: new FormControl(null),
      angkatan: new FormControl(null)
    });
  }

  editProfile() {
    let data = this.editProfileForm.value;

    let picName = this.authService.picture;

      // to random name for image
      this.imageFileName = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

      if(this.imageURI){
        picName = this.imageFileName + '.jpg';
        this.upload();
      }

    this.authService
      .editProfile(
        // this.user.user_id,
        // data.emailText,
        data.name,
        data.nim,
        data.prodi,
        data.angkatan,
        picName
      )
      .then(response => {
        // ambil hasil dari database
        console.log(response);
        if (response) {
          this.navCtrl.pop();
        } else {
        }
      });
  }

  backButton(){
    this.navCtrl.pop();
  }

  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
  
    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.imgUrl = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  takePhoto(){
    const options: CameraOptions = {
      quality: 100,
      encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE
    }
  
    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.imgUrl = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  upload(){
    // imageData is either a base64 encoded string or a file URI
    // If it's base64:

    const fileTransfer: FileTransferObject = this.transfer.create();

    let options1: FileUploadOptions = {
      fileKey: 'file',
      fileName: this.imageFileName + '.jpg',
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    
    }

    fileTransfer.upload(this.imageURI, 'http://misaelazarya.com/Mobile2/Project/upload.php', options1)
    .then((data) => {
        // success
      swal("Berhasil!","","success");
    }, (err) => {
        // error
      alert("error"+JSON.stringify(err));
    });
  }  
  
}
