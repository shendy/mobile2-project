import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { LecturerMainPage } from '../lecturer-main/lecturer-main';
import { ToDoListPage } from '../to-do-list/to-do-list';
import { StudentMainPage } from '../student-main/student-main';
import { StudentReqBimbinganPage } from '../student-req-bimbingan/student-req-bimbingan';

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  template: `
    <ion-tabs color="primary">
      <ion-tab [root]="studentMainPage" tabTitle="Beranda" tabIcon="home"></ion-tab>
      <ion-tab [root]="toDoListPage" tabTitle="Daftar Tugas" tabIcon="book"></ion-tab>
      <ion-tab [root]="studentReqBimbingan" tabTitle="Bimbingan" tabIcon="add-circle"></ion-tab>
    </ion-tabs>
  `
})
export class TabsPage {
  lecturerMainPage =  LecturerMainPage;
  toDoListPage = ToDoListPage;
  studentMainPage = StudentMainPage;
  studentReqBimbingan = StudentReqBimbinganPage;
  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController) {
  }

  chat() {
    // let modal = this.modalCtrl.create(LecturerMain2Page);
    // modal.present();
  }

}
