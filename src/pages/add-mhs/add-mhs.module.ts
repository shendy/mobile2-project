import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddMhsPage } from './add-mhs';

@NgModule({
  declarations: [
    AddMhsPage,
  ],
  imports: [
    IonicPageModule.forChild(AddMhsPage),
  ],
})
export class AddMhsPageModule {}
