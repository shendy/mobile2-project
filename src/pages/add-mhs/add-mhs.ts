import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  AlertController
} from "ionic-angular";
import { WebService } from "../../services/WebService";
import { LecturerService } from "../../services/LecturerService";
import { Mahasiswa } from "../../data/mahasiswa";
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: "page-add-mhs",
  templateUrl: "add-mhs.html"
})
export class AddMhsPage {
  listMhs: Mahasiswa[] = [];
  session: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private lecturerService: LecturerService,
    private alertCtrl: AlertController,
    private storage: Storage
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad AddMhsPage");
    // 1 untuk ambil mahasiswa aja
    this.storage.get("session").then(val => {
      this.session = val;
      this.lecturerService.getAllListMahasiswa(1, this.session).then(response => {
        // masukin ke variabel global untuk tampilin di html
        this.listMhs = response;
        console.log(this.listMhs);
      });
    });

    
  }
  
  chooseType(mhs: Mahasiswa) {
    const alert = this.alertCtrl.create({
      title: 'Choose type',
      buttons: [
        {
          text: 'Magang',
          handler: data => {
            this.submitMhs(mhs, 0);
          }
        },
        {
          text: 'Skripsi',
          handler: data => {
            this.submitMhs(mhs, 1);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log("Cancel choose type.");
          }
        }
      ]
    });

    alert.present();
  }

  submitMhs(mhs: Mahasiswa, type: number) {
    this.lecturerService.insertBimbingan(mhs.user_id, type).then(response => {
      if(response) {
        console.log("berhasil menambahkan bimbingan");
      }else{
        console.log("gagal menambahkan bimbingan");
      }
      this.viewCtrl.dismiss(); //menutup modal dan kembali kehalaman sebelumnya
    })
  }

  backButton(){
    this.navCtrl.pop();
  }
}
