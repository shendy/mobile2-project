import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StudentReqBimbinganPage } from './student-req-bimbingan';

@NgModule({
  declarations: [
    StudentReqBimbinganPage,
  ],
  imports: [
    IonicPageModule.forChild(StudentReqBimbinganPage),
  ],
})
export class StudentReqBimbinganPageModule {}
