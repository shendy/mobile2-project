import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { StudentService } from '../../services/StudentService';
import swal from "sweetalert2";


@IonicPage()
@Component({
  selector: 'page-student-req-bimbingan',
  templateUrl: 'student-req-bimbingan.html',
})
export class StudentReqBimbinganPage {
  reqDate;
  reqTime;
  type;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private studentService: StudentService) {
  }

  sendRequest(){
    this.storage.get("session").then(val => {
      let reqDateTime = this.reqDate + " " + this.reqTime + ":00";
      // console.log(reqDateTime);
      
      // Get schedule from database
      this.studentService.insertSchedule(val, reqDateTime, this.type, 0).then(response => {
        console.log(response);
        if(response){
          swal("Berhasil membuat janji", "", "success");
          // this.navCtrl.pop();
        }else {
          swal(""+response['error'], "", "warning");
        }
      });
    });
  }

}
