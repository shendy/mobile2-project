import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationLecturerPage } from './registration-lecturer';

@NgModule({
  declarations: [
    RegistrationLecturerPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationLecturerPage),
  ],
})
export class RegistrationLecturerPageModule {}
