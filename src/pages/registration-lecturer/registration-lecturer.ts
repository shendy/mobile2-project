import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/authService';
import {LoginPage} from '../../pages/login/login'
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import swal from "sweetalert2";

@IonicPage()
@Component({
  selector: 'page-registration-lecturer',
  templateUrl: 'registration-lecturer.html',
})
export class RegistrationLecturerPage {
  signUpForm: FormGroup;
  imageURI:any;
  imageFileName:any;
  imgUrl:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private authService: AuthService,
    private camera: Camera,
    private transfer: FileTransfer) {
  }

  ngOnInit() {
    this.initializeForm();
  }
  
  backButton(){
    this.navCtrl.pop();
  }

  private initializeForm() {
    this.signUpForm = new FormGroup({
      emailText: new FormControl(null, Validators.required),
      name: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      // picture: new FormControl(null, Validators.required),
    });
  }

  signUp() {
    let data = this.signUpForm.value;
      let email = data.emailText;
      let name = data.name;
      let password = data.password;
      let picName = 'user.jpg';

      // to random name for image
      this.imageFileName = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

      if(this.imageURI){
        picName = this.imageFileName + '.jpg';
        this.upload();
      }

      // panggil service untuk signup
      this.authService.signUpLecturer(email, name, password, picName);
      this.navCtrl.pop();
  }


  // go to login page
  login() {
    this.navCtrl.setRoot(LoginPage);
  }

  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
  
    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.imgUrl = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  takePhoto(){
    const options: CameraOptions = {
      quality: 100,
      encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE
    }
  
    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.imgUrl = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  upload(){
    // imageData is either a base64 encoded string or a file URI
    // If it's base64:

    const fileTransfer: FileTransferObject = this.transfer.create();

    let options1: FileUploadOptions = {
      fileKey: 'file',
      fileName: this.imageFileName + '.jpg',
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    
    }

    fileTransfer.upload(this.imageURI, 'http://misaelazarya.com/Mobile2/Project/upload.php', options1)
    .then((data) => {
        // success
      alert("success");
    }, (err) => {
        // error
      alert("error"+JSON.stringify(err));
    });
  }  
}
