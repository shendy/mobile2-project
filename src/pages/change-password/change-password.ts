import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from "../../services/authService";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Mahasiswa } from "../../data/mahasiswa";
import { SettingsPage } from '../settings/settings';

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {
  changePasswordForm: FormGroup;


  constructor(public navCtrl: NavController, public navParams: NavParams, private authService: AuthService) {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  ngOnInit() {
    this.initializeForm();
  }

  private initializeForm() {
    this.changePasswordForm = new FormGroup({
      emailText: new FormControl(null, Validators.required),
      NewPassword: new FormControl(null, Validators.required),
      RetypeNewPassword: new FormControl(null, Validators.required),
    });
  }

  changePassword() {
    let data = this.changePasswordForm.value;
      let email = data.emailText;
      let password = data.NewPassword;
      // panggil service untuk signup
      this.authService.changePassword(email, password).then(response => {
        this.navCtrl.pop();
      });
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  backButton(){
    this.navCtrl.pop();
  }

}
