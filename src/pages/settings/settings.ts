import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChangePasswordPage } from '../change-password/change-password';
import { EditProfilePage } from '../edit-profile/edit-profile';


@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  changePassword = ChangePasswordPage;
  editProfile = EditProfilePage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goToChangePassword() {
    this.navCtrl.push(this.changePassword);
  }

  goToEditProfile() {
    this.navCtrl.push(this.editProfile);
  }

}
