import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { StudentService } from "../../services/StudentService";

@IonicPage()
@Component({
  selector: "page-to-do-list",
  templateUrl: "to-do-list.html"
})
export class ToDoListPage {
  session: any;
  type: number;
  isMagang = true;
  listTask: any= [];

  constructor(
    public navCtrl: NavController,
    private storage: Storage,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private studentService: StudentService
  ) {}

  ionViewWillEnter() {
    this.storage.get("session").then(val => {
      console.log(val);
      this.session = val;
      this.type = this.session.position;
      this.getListTask();
    });
  }

  addTask() {
    let modal = this.modalCtrl.create("AddTaskPage");
    modal.present();
  }

  onLoad(type) {
    if (type == "magang") {
      document.getElementById("magang").classList.add("active");
      document.getElementById("skripsi").classList.remove("active");
      this.isMagang = true;
      this.getListTask();
    } else if (type == "skripsi") {
      document.getElementById("magang").classList.remove("active");
      document.getElementById("skripsi").classList.add("active");
      this.isMagang = false;
      this.getListTask();
    }
  }

  getListTask() {
    let typeBimbingan = (this.isMagang ? 0:1);
      this.studentService.getTodoList(this.session, typeBimbingan).then(response => {
        this.listTask =[];
        if(response != 'Error'){
          this.listTask = response;
        }
        console.log(this.listTask);
      });
  }
}
