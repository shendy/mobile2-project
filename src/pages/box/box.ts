import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {ChangePasswordPage} from '../change-password/change-password';


/**
 * Generated class for the BoxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-box',
  templateUrl: 'box.html',
})
export class BoxPage {

  BoxCodeForm : FormGroup;
  public getrandom;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  this.getrandom = navParams.get("number");
  }

  
  ionViewDidLoad() {
    console.log('ionViewDidLoad BoxPage');
  }

  ngOnInit() {
    this.initializeForm();
  }

  private initializeForm() {
    this.BoxCodeForm = new FormGroup({
      NumberText: new FormControl(null, Validators.required)
      
    });
  }

  BoxCodePage(){
    let data = this.BoxCodeForm.value;
    let NumberText = data.NumberText;
    let randomNumber = this.getrandom;
    // console.log(NumberText);
    // console.log(randomNumber);
    if(NumberText == randomNumber){
      this.navCtrl.push(ChangePasswordPage);
    }
    else{
      return false;
    }
    
  }

}
