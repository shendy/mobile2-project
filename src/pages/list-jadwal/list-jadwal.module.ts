import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListJadwalPage } from './list-jadwal';

@NgModule({
  declarations: [
    ListJadwalPage,
  ],
  imports: [
    IonicPageModule.forChild(ListJadwalPage),
  ],
})
export class ListJadwalPageModule {}
