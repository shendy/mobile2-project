import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';

/**
 * Generated class for the ListJadwalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-jadwal',
  templateUrl: 'list-jadwal.html',
})
export class ListJadwalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListJadwalPage');
  }

  detailMhs() {
    let alert = this.alertCtrl.create({
      title: 'Bimbingan bla bla',
      subTitle: 'Nama <br> tanggal',
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
