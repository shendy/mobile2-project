import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/authService';
import {BoxPage} from '../box/box';

/**
 * Generated class for the ForgetpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgetpassword',
  templateUrl: 'forgetpassword.html',
})
export class ForgetpasswordPage {
  
  ForgetpasswordForm : FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, private authService: AuthService){
  //this.randomNumber = Math.random()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetpasswordPage');
  }
  ngOnInit() {
    this.initializeForm();
  }

  private initializeForm() {
    this.ForgetpasswordForm = new FormGroup({
      emailtext: new FormControl(null, Validators.required)
      
    });
  }

  forgetpassword(){
    let data = this.ForgetpasswordForm.value;
    let email = data.emailtext;
    let randomNumber = 0;
    // panggil service untuk signup
    randomNumber = Math.floor(Math.random() * 9999);
    this.authService.forgetpassword(email , randomNumber);
    this.navCtrl.push(BoxPage , {
      number: randomNumber
    });

    //random number

    
    // get random(){

    //   var rand = Math.floor(Math.random()*20)+1;
      
    //   return rand;       
    //   }

  }

  backButton(){
    this.navCtrl.pop();
  }
}
