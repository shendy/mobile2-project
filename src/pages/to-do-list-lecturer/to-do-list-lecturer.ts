import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";
import { Mahasiswa } from "../../data/mahasiswa";
import { LecturerService } from "../../services/LecturerService";
import { Task } from "../../data/task";
import swal from "sweetalert2";
import { Storage } from "@ionic/storage";

@IonicPage()
@Component({
  selector: "page-to-do-list-lecturer",
  templateUrl: "to-do-list-lecturer.html"
})
export class ToDoListLecturerPage {
  mhs: Mahasiswa;
  listTask: any = [];
  isMagang: boolean;
  listMhsMagang: Task[] = [];
  fulllistMhsMagang: Task[] = [];
  listMhsSkripsi: Task[] = [];
  fulllistMhsSkripsi: Task[] = [];
  session: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private lecturerService: LecturerService,
    private modalCtrl: ModalController,
    private storage: Storage
  ) {}

  ngOnInit() {
    this.mhs = this.navParams.data;
    // console.log(this.mhs);

    this.storage.get("session").then(val => {
      // console.log(val);
      this.session = val;
      this.lecturerService.getTodoListMahasiswa(this.mhs, val).then(response => {
        this.listTask = [];
        this.listTask = response;
        // console.log(response);
        this.breakType(response);
        this.isMagang = true;
      });
    });
    
  }

  breakType(listMhs: any) {
    for (let i = 0; i < listMhs.length; i++) {
      if (listMhs[i].type == 0) {
        this.listMhsMagang.push(listMhs[i]);
        this.fulllistMhsMagang.push(listMhs[i]);
      } else {
        this.listMhsSkripsi.push(listMhs[i]);
        this.fulllistMhsSkripsi.push(listMhs[i]);
      }
    }
    console.log(this.listMhsMagang);
    console.log(this.fulllistMhsSkripsi);
  }

  setStatus(task: any, idx: number) {
    swal({
      title: "Apakah tugas " + task.description + " sudah selesai?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, selesai!",
      cancelButtonText: "Belum"
    }).then(result => {
      // jika ingin merubah status
      if (result.value) {
        console.log(task);
        this.lecturerService.setStatusDone(task).then(response => {
          if (response["success"]) {
            console.log(this.listTask);
            this.listTask[idx]["status_done"] = task.status_done == 0 ? 1 : 0;
            swal("Selesai!", task.description + " sudah selesai", "success");
          } else {
            swal(
              response["error"],
              task.description + " gagal dimasukkan",
              "warning"
            );
          }
        });
      }
    });
  }

  addTask() {
    console.log(this.listTask);
    let modal = this.modalCtrl.create("AddTaskLecturerPage", this.mhs);
    modal.present();
    modal.onDidDismiss(
      (task: Task) => {
        if(task) {
          this.listTask.push(task);
        }else{
          // kalo ga ada
        }
      }
    );
  }

  removeTask(task: any, idx: number){
    swal({
      title: "Apakah tugas " + task.description + " ingin dihapus?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya!",
      cancelButtonText: "Tidak"
    }).then(result => {
      // jika ingin merubah status
      if (result.value) {
        this.lecturerService.deleteBimbinganDetail(task).then(response => {
          if (response["success"]) {
            this.listTask.splice(idx, 1);
            swal("Berhasil!", task.description + " berhasil dihapus", "success");
          } else {
            swal(
              response["error"],
              task.description + " gagal menghapus",
              "warning"
            );
          }
        });
      }
    });
  }

  onLoad(type) {
    if (type == "magang") {
      document.getElementById("magang").classList.add("active");
      document.getElementById("skripsi").classList.remove("active");
      this.isMagang = true;
    } else if (type == "skripsi") {
      document.getElementById("magang").classList.remove("active");
      document.getElementById("skripsi").classList.add("active");
      this.isMagang = false;
    }
  }
}
