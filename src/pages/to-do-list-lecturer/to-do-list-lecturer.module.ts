import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ToDoListLecturerPage } from './to-do-list-lecturer';

@NgModule({
  declarations: [
    ToDoListLecturerPage,
  ],
  imports: [
    IonicPageModule.forChild(ToDoListLecturerPage),
  ],
})
export class ToDoListLecturerPageModule {}
