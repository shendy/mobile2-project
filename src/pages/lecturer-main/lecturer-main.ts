import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController, Platform } from "ionic-angular";
import { WebService } from "../../services/WebService";
import { Headers } from "@angular/http";
import { ListMhsPage } from "../list-mhs/list-mhs";
import { BankBimbinganPage } from "../bank-bimbingan/bank-bimbingan";
import { Mahasiswa } from "../../data/mahasiswa";
import { Storage } from "@ionic/storage";
import { LecturerService } from "../../services/LecturerService";
import * as moment from "moment";
import swal from "sweetalert2";
import { ListJadwalPage } from "../list-jadwal/list-jadwal";
import { ReqBimbinganPage } from "../req-bimbingan/req-bimbingan";
// untuk notif
import { LocalNotifications } from '@ionic-native/local-notifications';

/**Referensi
 * calendar => https://www.npmjs.com/package/ionic2-calendar
 */

@IonicPage()
@Component({
  selector: "page-lecturer-main",
  templateUrl: "lecturer-main.html"
})
export class LecturerMainPage {
  dosen: Mahasiswa;
  eventSource;
  lectureSchedule: any;
  isToday: boolean;
  viewTitle;
  calendar = {
    mode: "month",
    currentDate: new Date()
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public webService: WebService,
    public plt: Platform,
    private storage: Storage,
    private lecturerService: LecturerService,
    private alertCtrl: AlertController,
    private localNotifications: LocalNotifications
  ) {}

  /**Setiap page akan ditampilkan maka otomatis jalan */
  ionViewWillEnter() {
    this.storage.get("session").then(val => {
      // Get data untuk informasi user
      this.dosen = val;

      // Get schedule from mysql
      this.lecturerService.getSchedule(val).then(response => {
        this.lectureSchedule = response;
        console.log(this.lectureSchedule);
        this.eventSource = this.setInfo();
      });
    });
  }

  // ngOnInit() {
  //  this.storage.get("session").then(val => {
  //       this.dosen = val;
  //       console.log(this.dosen);
  //     });
  //   // let header = new Headers({
  //   "Content-Type": "application/json"
  // });
  // this.webService
  //   // .post(this.webService.url+"get.php",JSON.stringify(req), header)
  //   .get(this.webService.url+"get.php", header)
  //   .subscribe(
  //     response => {
  //       if (response == null) {
  //       } else {
  //         let responseData: any = JSON.parse(response["_body"]);
  //         if (responseData == null) {
  //         } else {
  //           // console.log(responseData['data']);
  //         }
  //       }
  //     },
  //     error => {}
  //   );
  // }

  /**
   * Set Info untuk jadwal di kalender
   * 
   * @returns events Array
   */
  setInfo() {
    var events = [];
    var d = new Date();

    for (let idx = 0; idx < this.lectureSchedule.length; idx++) {
      var startTime = new Date(this.lectureSchedule[idx]["start_time"]);
      var endTime = new Date(this.lectureSchedule[idx]["end_time"]);
      
      var tgl_bimbingan = startTime.getDate();
      var bln_bimbingan = startTime.getMonth();
      var thn_bimbingan = startTime.getFullYear();
      var jam_bimbingan = startTime.getHours();
      var menit_bimbingan = startTime.getMinutes();
      var tgl_sekarang = d.getDate();
      var bln_sekarang = d.getMonth();
      var thn_sekarang = d.getFullYear();
      var jam_sekarang = d.getHours();
      if(jam_sekarang < jam_bimbingan && tgl_sekarang==tgl_bimbingan && bln_sekarang==bln_bimbingan && thn_sekarang==thn_bimbingan){
        console.log(idx);
        this.localNotifications.schedule({
          id: idx,
          text: jam_bimbingan + ":" + menit_bimbingan + ": Ada Bimbingan " + (this.lectureSchedule[idx]["type"] == 0 ? 'Magang':'Skripsi') + " dengan " + this.lectureSchedule[idx]["name"],
          vibrate: true,
          sound: this.plt.is('android') ? 'file://sound.mp3': 'file://beep.caf',
        });
      }

      events.push({
        title:
          "Bimbingan " +
          (this.lectureSchedule[idx]["type"] == 0 ? 'Magang':'Skripsi') +
          " dengan " +
          this.lectureSchedule[idx]["name"],
        startTime: startTime,
        endTime: endTime,
        allDay: false
      });
    }

    return events;
  }

  /**
   * Jika user memilih tanggal
   * 
   * @param event Date
   */
  onCurrentDateChanged(event: Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();
  }

  /**
   * Jika user memilih event
   * 
   * @param event Array
   */
  onEventSelected(event) {
    console.log(event);
    //console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
    let start = moment(event.startTime).format("LLLL");
    let end = moment(event.endTime).format("LLLL");

    let alert = this.alertCtrl.create({
    title: '' + event.title,
    subTitle: '<br>From: ' + start + '<br>To: ' + end,
    buttons: ['OK']
    })
    alert.present();

    // swal("" + event.title, "", "info");
  }

  /**
   * Jika bulan diganti 
   * 
   * @param title string
   */
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  // Info jika tanggal dipilih
  onTimeSelected(ev) {
    console.log(
      "Selected time: " +
        ev.selectedTime +
        ", hasEvents: " +
        (ev.events !== undefined && ev.events.length !== 0) +
        ", disabled: " +
        ev.disabled
    );
  }

  // changeMode(mode) {
  //   console.log(mode);
  //   this.calendar.mode = mode;
  // }

  // onRangeChanged(ev) {
  //   console.log(
  //     "range changed: startTime: " + ev.startTime + ", endTime: " + ev.endTime
  //   );
  // }

  // markDisabled = (date: Date) => {
  //   var current = new Date();
  //   current.setHours(0, 0, 0);
  //   return date < current;
  // };

  goToListJadwalPage() {
    this.navCtrl.push(ListJadwalPage);
  }
  goToListMhsPage() {
    this.navCtrl.push(ListMhsPage);
  }

  goToBankBimbingan() {
    this.navCtrl.push(BankBimbinganPage);
  }
  goToReqBimbinganPage(){
    this.navCtrl.push(ReqBimbinganPage);
  }
}