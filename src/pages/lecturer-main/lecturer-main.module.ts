import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LecturerMainPage } from './lecturer-main';

@NgModule({
  declarations: [
    LecturerMainPage,
  ],
  imports: [
    IonicPageModule.forChild(LecturerMainPage),
  ],
})
export class LecturerMainPageModule {}
