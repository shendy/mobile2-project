import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationStudentPage } from './registration-student';

@NgModule({
  declarations: [
    RegistrationStudentPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationStudentPage),
  ],
})
export class RegistrationStudentPageModule {}
