import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/authService';
import {LoginPage} from '../../pages/login/login'
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@IonicPage()
@Component({
  selector: 'page-registration-student',
  templateUrl: 'registration-student.html',
})
export class RegistrationStudentPage {
  signUpForm: FormGroup;
  imageURI:any;
  imageFileName:any;
  imgUrl:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private authService: AuthService,
    private camera: Camera,
    private transfer: FileTransfer) {
  }

  ngOnInit() {
    this.initializeForm();
  }

  private initializeForm() {
    this.signUpForm = new FormGroup({
      emailText: new FormControl(null, Validators.required),
      name: new FormControl(null, Validators.required),
      nim: new FormControl(null, Validators.required),
      prodi: new FormControl(null, Validators.required),
      angkatan: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

  signUp() {
    let data = this.signUpForm.value;
    console.log(data);
    let email = data.emailText;
    let name = data.name;
    let nim = data.nim;
    let prodi = data.prodi;
    let angkatan = data.angkatan;
    let password = data.password;

    let picName = 'user.jpg';

      // to random name for image
      this.imageFileName = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

      if(this.imageURI){
        picName = this.imageFileName + '.jpg';
        this.upload();
      }
    
    // panggil service untuk signup
    this.authService.signupStudent(email, name, nim, prodi, angkatan,password, picName);
    this.navCtrl.pop();
      
  }

  back() {
    this.navCtrl.pop();
  }

  backButton(){
    this.navCtrl.pop();
  }

  
  // go to login page
  login() {
    this.navCtrl.setRoot(LoginPage);
  }

  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
  
    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.imgUrl = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  takePhoto(){
    const options: CameraOptions = {
      quality: 100,
      encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE
    }
  
    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.imgUrl = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  upload(){
    // imageData is either a base64 encoded string or a file URI
    // If it's base64:

    const fileTransfer: FileTransferObject = this.transfer.create();

    let options1: FileUploadOptions = {
      fileKey: 'file',
      fileName: this.imageFileName + '.jpg',
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    
    }

    fileTransfer.upload(this.imageURI, 'http://misaelazarya.com/Mobile2/Project/upload.php', options1)
    .then((data) => {
        // success
      alert("success");
    }, (err) => {
        // error
      alert("error"+JSON.stringify(err));
    });
  }  
  
}
