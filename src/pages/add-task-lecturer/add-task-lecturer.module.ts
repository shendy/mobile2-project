import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddTaskLecturerPage } from './add-task-lecturer';

@NgModule({
  declarations: [
    AddTaskLecturerPage,
  ],
  imports: [
    IonicPageModule.forChild(AddTaskLecturerPage),
  ],
})
export class AddTaskLecturerPageModule {}
