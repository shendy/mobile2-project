import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { LecturerService } from "../../services/LecturerService";
import swal from "sweetalert2";
import { Mahasiswa } from '../../data/mahasiswa';

@IonicPage()
@Component({
  selector: "page-add-task-lecturer",
  templateUrl: "add-task-lecturer.html"
})
export class AddTaskLecturerPage {
  addTaskForm: FormGroup;
  mhs: Mahasiswa;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private lecturerService: LecturerService
  ) {}

  ngOnInit() {
    this.mhs = this.navParams.data;
    console.log(this.mhs);
    this.initializeForm();
  }

  private initializeForm() {
    this.addTaskForm = new FormGroup({
      desc: new FormControl(null, Validators.required),
      weight: new FormControl(null, Validators.required)
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  add() {
    let data = this.addTaskForm.value;
    // console.log(data);
    this.lecturerService
      .insertBimbinganDetail(data, this.mhs)
      .then(response => {
        console.log(response);
        if (response["success"]) {
          console.log("berhasil menambahkan bimbingan");
          swal("Berhasil menambahkan bimbingan", "", "success");
          this.viewCtrl.dismiss(response["data"][0]); //menutup modal dan kembali kehalaman sebelumnya
        } else {
          console.log("gagal menambahkan bimbingan");
          swal("Berhasil menambahkan bimbingan", "", "warning");
          this.dismiss();
        }
      });
  }

  backButton() {
    this.navCtrl.pop();
  }
}
