import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormBimbinganPage } from './form-bimbingan';

@NgModule({
  declarations: [
    FormBimbinganPage,
  ],
  imports: [
    IonicPageModule.forChild(FormBimbinganPage),
  ],
})
export class FormBimbinganPageModule {}
