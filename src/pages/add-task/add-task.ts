import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { LecturerService } from "../../services/LecturerService";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import swal from "sweetalert2";
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
  selector: "page-add-task",
  templateUrl: "add-task.html"
})
export class AddTaskPage {
  addTaskForm: FormGroup;
  session: any;
  type;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private lecturerService: LecturerService,
    private storage: Storage
  ) {
  }

  ngOnInit() {
    this.initializeForm();
    this.storage.get("session").then(val => {
      this.session = val;
    });
  }

  private initializeForm() {
    this.addTaskForm = new FormGroup({
      desc: new FormControl(null, Validators.required),
      weight: new FormControl(null, Validators.required),
      type: new FormControl(null, Validators.required),
    });
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }

  add() {
    let data = this.addTaskForm.value;
    console.log(data);
    this.lecturerService.insertBimbinganCollection(data.desc, data.weight, this.session, data.type).then(response => {
      console.log(response);
      if (response["success"]) {
        console.log("berhasil menambahkan bimbingan");
        swal("Berhasil menambahkan bimbingan","","success");
        this.viewCtrl.dismiss(response["data"][0]); //menutup modal dan kembali kehalaman sebelumnya
      } else {
        console.log("gagal menambahkan bimbingan");
        swal("Berhasil menambahkan bimbingan","","warning");
        this.dismiss();
      }
    });
  }

  backButton(){
    this.dismiss();
  }
}
