import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController,  App, ModalController, AlertController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/authService'
import { LecturerMainPage } from '../lecturer-main/lecturer-main';
import { StudentMainPage } from '../student-main/student-main';
import { RegistrationLecturerPage } from '../registration-lecturer/registration-lecturer';
import { RegistrationStudentPage } from '../registration-student/registration-student';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ForgetpasswordPage } from '../forgetpassword/forgetpassword';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  logInForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    private authService: AuthService,
    private http: Http,private app:App) {}

  /**
   * Pertama kali dijalankan
   */
  ngOnInit() {
    this.initializeForm();
  }

  /**
   * Untuk validator saat di form
   */
  private initializeForm() {
    this.logInForm = new FormGroup({
      emailText: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

  /**
   * Saat button login dijalankan
   */
  logIn() {
    console.log(this.logInForm.value);
      let email = this.logInForm.value.emailText;
      let password = this.logInForm.value.password;
      
      var isSuccess = this.authService.login(email, password, (page:any)=>{this.app.getRootNav().setRoot(page);});
  }

  /**
   * Go to Registration Lecturer Page
   */
  goRegistrationLecturer() {
    this.navCtrl.push(RegistrationLecturerPage);
  }

  /**
   * Go to Registration Student Page
   */
  goRegistrationStudent() {
    this.navCtrl.push(RegistrationStudentPage);
  }
  // forgotPass(){
  //   let alert = this.alertCtrl.create({
  //     title: 'Lupa Password?',
  //     subTitle: 'Masukkan alamat email kamu untuk mengirim password baru',
  //     inputs: [
  //       {
  //         name: 'email',
  //         placeholder: 'Email'
  //       }
  //     ],
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         handler: data => {
  //           console.log('Cancel clicked');
  //         }
  //       },
  //       {
  //         text: 'Kirim',
  //         handler: data => {
  //           console.log('Kirim clicked');
  //         }
  //       }
  //     ]
  //   });
  //   alert.present();
  // }

  forgotPass(){
    let modal = this.modalCtrl.create(ForgetpasswordPage);
    modal.present();
  }
}
