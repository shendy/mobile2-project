import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StudentMainPage } from './student-main';

@NgModule({
  declarations: [
    StudentMainPage,
  ],
  imports: [
    IonicPageModule.forChild(StudentMainPage),
  ],
})
export class StudentMainPageModule {}
