import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Mahasiswa } from '../../data/mahasiswa';
import { AuthService } from '../../services/authService';
import { Storage } from "@ionic/storage";
import { StudentService } from '../../services/StudentService';
import { ToDoListPage } from '../to-do-list/to-do-list';
import { StudentReqBimbinganPage } from '../student-req-bimbingan/student-req-bimbingan';
import { LecturerService } from '../../services/LecturerService';

@IonicPage()
@Component({
  selector: 'page-student-main',
  templateUrl: 'student-main.html',
})
export class StudentMainPage {
  user: Mahasiswa;
  scheduleMahasiswa: any;
  numProgress: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private studentService: StudentService,  public alertCtrl: AlertController, private lecturerService: LecturerService) {
  }

  ionViewWillEnter() {
    this.storage.get("session").then(val => {
      // Get data untuk informasi user
      this.user = val;
      // Get schedule from database
      this.studentService.getScheduleMahasiswa(this.user).then(response => {
        this.scheduleMahasiswa = response;
      });

      this.studentService.getTodoList(this.user, 0).then(response => {
        let a=0 ;
        let listTask: any = [];
        listTask = response;
        let totalWeight = 0;

        console.log(listTask);
        for (let i = 0; i < listTask.length; i++) {
          if (listTask[i].status_done == 1) {
            a += +listTask[i].weight;
          }
          totalWeight += +listTask[i].weight;
        }
        if(a != 0 && totalWeight != 0){
          this.numProgress = (a/totalWeight)*100;
        }
      });

    });
  }

  showDetail(schedule: any){
    let alert = this.alertCtrl.create({
      title: 'Detail Bimbingan',
      subTitle: 'Bimbingan ' + (schedule.type == '0' ? 'Magang':'Skripsi') + " dengan " + schedule.name,
      buttons: ['Close']
    });
    alert.present();
  }

  goToToDoListPage(){
    this.navCtrl.push(ToDoListPage);
  }

  goToMhsReqBimbingan(){
    this.navCtrl.push(StudentReqBimbinganPage);
  }

}
