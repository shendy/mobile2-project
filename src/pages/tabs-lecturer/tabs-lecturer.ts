import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { LecturerMainPage } from '../lecturer-main/lecturer-main';
import { ListMhsPage } from '../list-mhs/list-mhs';
import { ReqBimbinganPage } from '../req-bimbingan/req-bimbingan';
import { BankBimbinganPage } from '../bank-bimbingan/bank-bimbingan';


import { ToDoListPage } from '../to-do-list/to-do-list';
import { StudentMainPage } from '../student-main/student-main';
import { StudentReqBimbinganPage } from '../student-req-bimbingan/student-req-bimbingan';


/**
 * Generated class for the TabsLecturerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs-lecturer',
  template: `
    <ion-tabs color="primary">
      <ion-tab [root]="lecturerMainPage" tabTitle="Beranda" tabIcon="home"></ion-tab>
      <ion-tab [root]="listMhsPage" tabTitle="Daftar Tugas" tabIcon="book"></ion-tab>
      <ion-tab [root]="reqBimbinganPage" tabTitle="Bimbingan" tabIcon="browsers"></ion-tab>
      <ion-tab [root]="bankBimbinganPage" tabTitle="Bank Bimbingan" tabIcon="archivebrowsers-circle"></ion-tab>
    </ion-tabs>
  `
})
export class TabsLecturerPage {
  lecturerMainPage:LecturerMainPage;
  listMhsPage: ListMhsPage;
  reqBimbinganPage: ReqBimbinganPage;
  bankBimbinganPage: BankBimbinganPage; 

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsLecturerPage');
  }

}
