import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsLecturerPage } from './tabs-lecturer';

@NgModule({
  declarations: [
    TabsLecturerPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsLecturerPage),
  ],
})
export class TabsLecturerPageModule {}
