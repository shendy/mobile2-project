import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";
import { LecturerService } from "../../services/LecturerService";
import { AddTaskPage } from "../add-task/add-task";
import { Task } from "../../data/task";
import { FormGroup } from "@angular/forms";
import { EditTaskPage } from "../edit-task/edit-task";
import swal from "sweetalert2";
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: "page-bank-bimbingan",
  templateUrl: "bank-bimbingan.html"
})
export class BankBimbinganPage {
  public tasks: Task[] = [];
  editTodoListForm = FormGroup;
  isMagang: boolean;
  listMhsMagang: Task[] = [];
  fulllistMhsMagang: Task[] = [];
  listMhsSkripsi: Task[] = [];
  fulllistMhsSkripsi: Task[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private lecturerService: LecturerService,
    private modalCtrl: ModalController,
    private storage: Storage
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad BankBimbinganPage");
    this.storage.get("session").then(val => {
      this.lecturerService.getBankBimbingan(val).then(response => {
        // ambil hasil dari database
        this.isMagang = true;
       
        if(response.length != 0){
          this.breakType(response)
        }
        console.log(this.listMhsMagang);
        console.log(this.listMhsSkripsi);
      });
    });
  }

  breakType(listMhs: any) {
    for (let i = 0; i < listMhs.length; i++) {
      if (listMhs[i].type == 0) {
        this.listMhsMagang.push(listMhs[i]);
        this.fulllistMhsMagang.push(listMhs[i]);
      } else {
        this.listMhsSkripsi.push(listMhs[i]);
        this.fulllistMhsSkripsi.push(listMhs[i]);
      }
    }
  }

  presentAddModal() {
    let addModal = this.modalCtrl.create(AddTaskPage);
    addModal.present();
    addModal.onDidDismiss(
      (task: any) => {
        console.log(task);
        if(task != null){
          if(task.type == 0) {
            this.listMhsMagang.push(task);
          }
          else if(task.type == 1){
            this.listMhsSkripsi.push(task);
          }
          else{
            // kalo ga ada
            swal("Gagal!","","error");
          }
        }
    });
  }

  editTask(task: Task, idx: number) {
    let addModal = this.modalCtrl.create(EditTaskPage, { task: task });
    addModal.present();
    addModal.onDidDismiss(
      (task: any) => {
        if(task) {
          this.tasks[idx] = task;
        }else{
          swal("Gagal!","","error");
        }
    });
  }

  onLoad(type) {
    if (type == "magang") {
      document.getElementById("magang").classList.add("active");
      document.getElementById("skripsi").classList.remove("active");
      this.isMagang = true;
    } else if (type == "skripsi") {
      document.getElementById("magang").classList.remove("active");
      document.getElementById("skripsi").classList.add("active");
      this.isMagang = false;
    }
  }
}
