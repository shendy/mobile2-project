import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BankBimbinganPage } from './bank-bimbingan';

@NgModule({
  declarations: [
    BankBimbinganPage,
  ],
  imports: [
    IonicPageModule.forChild(BankBimbinganPage),
  ],
})
export class BankBimbinganPageModule {}
