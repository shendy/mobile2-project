import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Task } from "../../data/task";
import { LecturerService } from "../../services/LecturerService";
import swal from "sweetalert2";

@IonicPage()
@Component({
  selector: "page-edit-task",
  templateUrl: "edit-task.html"
})
export class EditTaskPage {
  editTaskForm: FormGroup;
  task: Task;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private lecturerService: LecturerService
  ) {
    this.task = this.navParams.get('task');
    console.log(this.task);
  }

  ngOnInit() {
    this.initializeForm();
  }

  private initializeForm() {
    this.editTaskForm = new FormGroup({
      desc: new FormControl(null, Validators.required),
      sort: new FormControl(null, Validators.required),
      weight: new FormControl(null, Validators.required)
    });
  }

  editTask() {
    let data = this.editTaskForm.value;
    this.task.description = data.desc;
    this.task.sort = data.sort;
    this.task.weight = data.weight;

    this.lecturerService
      .updateBimbinganCollection(this.task)
      .then(response => {
        console.log(response);
        if (response["success"]) {
          swal("Berhasil edit task","","success");
          this.viewCtrl.dismiss(this.task); //menutup modal dan kembali kehalaman sebelumnya
        } else {
          swal("Gagal edit task","","warning");
          this.dismiss();
        }
      });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
