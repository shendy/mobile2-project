import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfoMhsPage } from './info-mhs';

@NgModule({
  declarations: [
    InfoMhsPage,
  ],
  imports: [
    IonicPageModule.forChild(InfoMhsPage),
  ],
})
export class InfoMhsPageModule {}
