import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { Mahasiswa } from "../../data/mahasiswa";
import { ToDoListLecturerPage } from "../to-do-list-lecturer/to-do-list-lecturer";
import { FormBimbinganPage } from "../form-bimbingan/form-bimbingan";
import { LecturerService } from "../../services/LecturerService";
import { Storage } from "@ionic/storage";

@IonicPage()
@Component({
  selector: "page-info-mhs",
  templateUrl: "info-mhs.html"
})
export class InfoMhsPage {
  mhs: Mahasiswa;
  listTask: any = [];
  numProgress: number = 0;
  session: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private lecturerService: LecturerService,
    private storage: Storage
  ) {}

  ngOnInit() {
    this.mhs = this.navParams.data;
  }

  ionViewWillEnter() {
    this.storage.get("session").then(val => {
      this.session = val;

      this.lecturerService
        .getUntukMahasiswa(this.mhs)
        .then(response => {
          this.listTask = [];
          this.listTask = response;
          let a = 0;

          let totalWeight = 0;
          for (let i = 0; i < this.listTask.length; i++) {
            if (this.listTask[i].status_done == 1) {
              a += +this.listTask[i].weight;
            }
            totalWeight += +this.listTask[i].weight;
          }
          if (a != 0 && totalWeight != 0) {
            this.numProgress = (a / totalWeight) * 100;
          }
        });
    });
  }

  goToToDoListPage() {
    this.navCtrl.push(ToDoListLecturerPage, this.mhs);
  }

  // goToFormBimbingan(){
  //   this.navCtrl.push(FormBimbinganPage);
  // }
}
