import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListMhsPage } from './list-mhs';

@NgModule({
  declarations: [
    ListMhsPage,
  ],
  imports: [
    IonicPageModule.forChild(ListMhsPage),
  ],
})
export class ListMhsPageModule {}
