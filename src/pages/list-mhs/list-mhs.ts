import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";
import { WebService } from "../../services/WebService";
import { Headers } from "@angular/http";
import { LecturerService } from "../../services/LecturerService";
import { Mahasiswa } from "../../data/mahasiswa";
import { InfoMhsPage } from "../info-mhs/info-mhs";
import { Storage } from "@ionic/storage";
import { FormControl } from "@angular/forms";
import "rxjs/add/operator/debounceTime";
import swal from "sweetalert2";

@IonicPage()
@Component({
  selector: "page-list-mhs",
  templateUrl: "list-mhs.html"
})
export class ListMhsPage {
  listMhsMagang: Mahasiswa[] = [];
  fulllistMhsMagang: Mahasiswa[] = [];
  listMhsSkripsi: Mahasiswa[] = [];
  fulllistMhsSkripsi: Mahasiswa[] = [];
  isMagang: boolean;
  session: any;
  searchTerm: string = "";
  searchControl: FormControl;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private webService: WebService,
    private lecturerService: LecturerService,
    private storage: Storage
  ) {
    this.searchControl = new FormControl();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ListMhsPage");
    // console.log(this.navParams.data);
    this.setFilteredItems();

    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
      this.setFilteredItems();
    });
  }

  /** Masuk ke page akan mengambil data listMhs yang aktiv dengan dosen yang bersangkutan */
  ngOnInit() {
    this.getMhs();
  }

  getMhs() {
    this.listMhsMagang = [];
    this.fulllistMhsMagang = [];
    this.listMhsSkripsi = [];
    this.fulllistMhsSkripsi = [];

    this.storage.get("session").then(val => {
      // console.log(val);
      this.session = val;
      this.lecturerService.getListMahasiswa(this.session).then(response => {
        this.breakType(response);
        this.isMagang = true;
      });
    });
  }

  breakType(listMhs: any) {
    for (let i = 0; i < listMhs.length; i++) {
      if (listMhs[i].type == 0) {
        this.listMhsMagang.push(listMhs[i]);
        this.fulllistMhsMagang.push(listMhs[i]);
      } else {
        this.listMhsSkripsi.push(listMhs[i]);
        this.fulllistMhsSkripsi.push(listMhs[i]);
      }
    }
  }

  onLoad(type) {
    if (type == "magang") {
      document.getElementById("magang").classList.add("active");
      document.getElementById("skripsi").classList.remove("active");
      this.isMagang = true;
    } else if (type == "skripsi") {
      document.getElementById("magang").classList.remove("active");
      document.getElementById("skripsi").classList.add("active");
      this.isMagang = false;
    }
  }

  /** Menambahkan mahasiswa dengan membuat modal */
  addMhs() {
    let modal = this.modalCtrl.create("AddMhsPage");
    modal.present();
    modal.onDidDismiss((mhs: Mahasiswa) => {
      this.getMhs();
    });
  }

  detailMhs(mhs: Mahasiswa) {
    this.navCtrl.push(InfoMhsPage, mhs);
  }

  // untuk search
  setFilteredItems() {
    if (this.isMagang) {
      this.listMhsMagang = this.fulllistMhsMagang;
      this.listMhsMagang = this.filterItems(this.searchTerm);
    } else {
      this.listMhsSkripsi = this.fulllistMhsSkripsi;
      this.listMhsSkripsi = this.filterItems(this.searchTerm);
    }
  }

  filterItems(searchTerm) {
    if (this.isMagang) {
      return this.fulllistMhsMagang.filter(item => {
        return item.name.indexOf(searchTerm) > -1;
      });
    } else {
      return this.fulllistMhsSkripsi.filter(item => {
        return item.name.indexOf(searchTerm) > -1;
      });
    }
  }

  removeMahasiswa(mhs) {
    swal({
      title: "Apakah mahasiswa " + mhs.name + " ingin dihapus?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
      cancelButtonText: "Tidak"
    }).then(result => {
      // jika ingin merubah status
      if (result.value) {
        this.lecturerService
          .removeMahasiswa(mhs, this.session)
          .then(response => {
            this.getMhs();
          });
      }
    });
  }
}
