import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from "ionic-angular";
import { LecturerService } from "../../services/LecturerService";
import { Storage } from '@ionic/storage';
import { Mahasiswa } from '../../data/mahasiswa';
import swal from "sweetalert2";

@IonicPage()
@Component({
  selector: "page-req-bimbingan",
  templateUrl: "req-bimbingan.html"
})
export class ReqBimbinganPage {
  listRequest: any;
  lecturer: Mahasiswa;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private lecturerService: LecturerService,
    private storage: Storage
  ) {}

  ionViewWillEnter() {
    this.storage.get("session").then(val => {
      this.lecturer = val;
      // Get schedule from mysql
      this.lecturerService.getRequestBimbingan(val).then(response => {
        this.listRequest = response;
        console.log(this.listRequest);
      });
    });
  }

  showDetail(req: any, index: any) {
    const alert = this.alertCtrl.create({
      title: "Permintaan Bimbingan " + req.name,
      message: "Apakah ingin diterima?",
      cssClass:'buttonCss',
      buttons: [
        {
          text: "Accept",
          cssClass: 'btnAccept',
          handler: () => {
            this.lecturerService.setStatusSchedule(req, 1).then(response => {
              if(response['success']){
                this.listRequest.splice(index, 1);
                swal("Berhasil menerima janji", "", "success");
              }else{
                swal(""+response['error'], "", "warning");
              }
            });
          }
        },
        {
          text: "Reject",
          cssClass: 'btnReject',
          handler: () => {
            this.lecturerService.setStatusSchedule(req, 2).then(response => {
              if(response['success']){
                this.listRequest.splice(index, 1);
                swal("Berhasil menolak janji", "", "success");
              }else{
                swal(""+response['error'], "", "warning");
              }
            });
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            // console.log("NO is clicked");
          }
        }
      ]
    });
    alert.present();
  }
}
