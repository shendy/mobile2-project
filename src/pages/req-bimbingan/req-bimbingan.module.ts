import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReqBimbinganPage } from './req-bimbingan';

@NgModule({
  declarations: [
    ReqBimbinganPage,
  ],
  imports: [
    IonicPageModule.forChild(ReqBimbinganPage),
  ],
})
export class ReqBimbinganPageModule {}
