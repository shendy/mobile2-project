import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CalendarModule } from 'ionic3-calendar-en';
import { ProgressBarModule } from "angular-progress-bar"
import { StatusBar } from '@ionic-native/status-bar';
// Untuk calendar
import { NgCalendarModule  } from 'ionic2-calendar';

import { MyApp } from './app.component';

// http module
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
// Ionic Storage untuk session
import { IonicStorageModule } from '@ionic/storage';

//untuk camera dan file transfer
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

// Import page yang dipakai
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { LecturerMainPage } from '../pages/lecturer-main/lecturer-main';
import { StudentMainPage } from '../pages/student-main/student-main';
import { RegistrationLecturerPage } from '../pages/registration-lecturer/registration-lecturer';
import { RegistrationStudentPage } from '../pages/registration-student/registration-student';
import { TabsPage } from '../pages/tabs/tabs'; 
import { ListMhsPage } from '../pages/list-mhs/list-mhs';
import { InfoMhsPage } from '../pages/info-mhs/info-mhs';
import { ToDoListPage } from '../pages/to-do-list/to-do-list';
import { ReqBimbinganPage } from '../pages/req-bimbingan/req-bimbingan';
import { StudentReqBimbinganPage } from '../pages/student-req-bimbingan/student-req-bimbingan';
import { BankBimbinganPage } from '../pages/bank-bimbingan/bank-bimbingan';
import { AddTaskPage } from '../pages/add-task/add-task';
import { SettingsPage } from '../pages/settings/settings';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { ForgetpasswordPage } from '../pages/forgetpassword/forgetpassword';
import { BoxPage } from '../pages/box/box';
import { FormBimbinganPage } from '../pages/form-bimbingan/form-bimbingan';
import { ListJadwalPage } from '../pages/list-jadwal/list-jadwal'; 
import { ToDoListLecturerPage } from '../pages/to-do-list-lecturer/to-do-list-lecturer';
import { TabsLecturerPage } from '../pages/tabs-lecturer/tabs-lecturer';



// Services yang dipakai
import { AuthService } from '../services/authService';
import { WebService } from '../services/WebService';
import { LecturerService } from '../services/LecturerService';
import { StudentService } from '../services/StudentService';
import { AddTaskLecturerPageModule } from '../pages/add-task-lecturer/add-task-lecturer.module';
import { EditTaskPage } from '../pages/edit-task/edit-task';
import { LocalNotifications } from '@ionic-native/local-notifications';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    LecturerMainPage,
    StudentMainPage,
    RegistrationLecturerPage,
    RegistrationStudentPage,
    TabsPage,
    ListMhsPage,
    InfoMhsPage,
    ToDoListPage,
    ReqBimbinganPage,
    StudentReqBimbinganPage,
    BankBimbinganPage,
    AddTaskPage,
    SettingsPage,
    ChangePasswordPage,
    EditProfilePage,
    ForgetpasswordPage,
    FormBimbinganPage,
    ListJadwalPage,
    BoxPage, //buat masukin random number
    ToDoListLecturerPage,
    TabsLecturerPage,
    EditTaskPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule,
    CalendarModule,
    ProgressBarModule,
    NgCalendarModule,
    IonicStorageModule.forRoot(),
    AddTaskLecturerPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    LecturerMainPage,
    StudentMainPage,
    RegistrationLecturerPage,
    RegistrationStudentPage,
    TabsPage,
    ListMhsPage,
    InfoMhsPage,
    ToDoListPage,
    ReqBimbinganPage,
    StudentReqBimbinganPage,
    BankBimbinganPage,
    AddTaskPage,
    SettingsPage,
    ChangePasswordPage,
    EditProfilePage,
    ForgetpasswordPage,
    FormBimbinganPage,
    ListJadwalPage,
    BoxPage,
    ToDoListLecturerPage,
    TabsLecturerPage,
    EditTaskPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WebService,
    AuthService,
    LecturerService,
    StudentService,
    Camera,
    FileTransfer,
    LocalNotifications
  ]
})
export class AppModule {}
