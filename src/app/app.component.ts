import { Component, ViewChild } from "@angular/core";
import { Platform, NavController, MenuController, App } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
// untuk timer splash screen
import { timer } from 'rxjs/observable/timer';

// services
import { Storage } from "@ionic/storage";

/**
 * Import page yang dibutuhkan
 */
import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { TabsPage } from "../pages/tabs/tabs";
import { TabsLecturerPage } from "../pages/tabs-lecturer/tabs-lecturer";
import { LecturerMainPage } from "../pages/lecturer-main/lecturer-main";
import { ListMhsPage } from "../pages/list-mhs/list-mhs";
import { InfoMhsPage } from "../pages/info-mhs/info-mhs";
import { ToDoListPage } from "../pages/to-do-list/to-do-list";
import { ReqBimbinganPage } from "../pages/req-bimbingan/req-bimbingan";
import { StudentMainPage } from "../pages/student-main/student-main";
import { StudentReqBimbinganPage } from "../pages/student-req-bimbingan/student-req-bimbingan";
import { AuthService } from "../services/authService";
import { SettingsPage } from "../pages/settings/settings";
import { EditProfilePage } from "../pages/edit-profile/edit-profile"
import { ChangePasswordPage } from "../pages/change-password/change-password"
import { EditTaskPage } from '../pages/edit-task/edit-task'
import { Events } from 'ionic-angular';

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  
  showSplash = true; // <-- show animation

  isLogin = 0;

  gambar: string;
  nama: string;

  // Page dasar untuk dibuka saat dijalankan
  rootPage: any = LoginPage;
  // settingsPage = SettingsPage;
  // rootPage:any = LecturerMainPage;

  editProfilePage = EditProfilePage;
  changePasswordPage = ChangePasswordPage;

  @ViewChild("sideMenuContent") navCtrl: NavController;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private menuCtrl: MenuController,
    private storage: Storage,
    private authService: AuthService,
    private app: App,
    public events: Events
  ) {
    events.subscribe('edit:success', () => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.storage.get("session").then(value => {
        // console.log(value);
        if (
          value != null &&
          value.name != "" &&
          value.user_id != "" &&
          value.position != ""
        ) {
          this.authService.name = value.name;
          this.authService.user_id = value.user_id;
          this.authService.position = value.position;
          this.authService.email = value.email;
          this.authService.prodi = value.prodi;
          this.authService.nim = value.nim;
          this.authService.angkatan = value.angkatan;
          this.authService.picture = value.picture;
          this.nama = value.name;
          this.gambar = value.picture;
        }
    });
  });
  
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      timer(3000).subscribe(() => this.showSplash = false) // <-- hide animation after 3s
    });

    //session
    this.storage.get("session").then(value => {
      // console.log(value);
      if (
        value != null &&
        value.name != "" &&
        value.user_id != "" &&
        value.position != ""
      ) {
        this.authService.name = value.name;
        this.authService.user_id = value.user_id;
        this.authService.position = value.position;
        this.authService.email = value.email;
        this.authService.prodi = value.prodi;
        this.authService.nim = value.nim;
        this.authService.angkatan = value.angkatan;
        this.authService.picture = value.picture;
        this.nama = value.name;
        this.gambar = value.picture;
        console.log(this.authService.email);

        this.isLogin = 1;
        if (value.position == 0) {
          this.navCtrl.setRoot(LecturerMainPage);
        } else {
          this.navCtrl.setRoot(TabsPage);
        }
      } else {
        console.log(value);
        this.navCtrl.setRoot(LoginPage);
      }
    });
  }

  ionViewDidLoad(){
    //session
    this.storage.get("session").then(value => {
      // console.log(value);
      if (
        value != null &&
        value.name != "" &&
        value.user_id != "" &&
        value.position != ""
      ) {
        this.authService.name = value.name;
        this.authService.user_id = value.user_id;
        this.authService.position = value.position;
        this.authService.email = value.email;
        this.authService.prodi = value.prodi;
        this.authService.nim = value.nim;
        this.authService.angkatan = value.angkatan;
        this.authService.picture = value.picture;
        this.nama = value.name;
        this.gambar = value.picture;
        console.log(this.authService.email);

        this.isLogin = 1;
        if (value.position == 0) {
          this.navCtrl.setRoot(LecturerMainPage);
        } else {
          this.navCtrl.setRoot(TabsPage);
        }
      } else {
        console.log(value);
        this.navCtrl.setRoot(LoginPage);
      }
    });
  }

  onLoad(page: any) {
    this.navCtrl.push(page);
    this.menuCtrl.close();
  }

  logout() {
    console.log("logout");
    this.authService.logout(() => {
      this.app.getRootNav().setRoot(LoginPage);
    });
    this.isLogin = 0;
  }

}
