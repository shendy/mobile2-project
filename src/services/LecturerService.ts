import { Injectable } from "@angular/core";
import { WebService } from "./WebService";
import { Headers } from "@angular/http";
import { AlertController, LoadingController } from "ionic-angular";
import { LecturerMainPage } from "../pages/lecturer-main/lecturer-main";
import { StudentMainPage } from "../pages/student-main/student-main";
import { TabsPage } from "../pages/tabs/tabs";
import { Mahasiswa } from "../data/mahasiswa";
import { Task } from "../data/task";
import { Storage } from "@ionic/storage";

@Injectable()
export class LecturerService {
  session: any;

  constructor(
    private webService: WebService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private storage: Storage
  ) {}

  getProgress(mhs: Mahasiswa){
    // console.log(temp);
    let responseData: any;
    let url = this.webService.url + "getProgress.php";
    // Untuk dikirim ke API
    let requestData = {
      student_id: mhs.user_id,
      type: mhs.type
    };

    let header = new Headers({});

    return new Promise<[Mahasiswa]>(resolve => {
      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData.data);
          }
        });
    });
  }

  getListMahasiswa(temp: any) {
    this.session = temp;
    // console.log(temp);
    let responseData: any;
    let lecturer_id = this.session.user_id; // ambil dari session
    let url = this.webService.url + "getListMhs.php";
    // Untuk dikirim ke API
    let requestData = {
      lecturer_id: lecturer_id
    };

    let header = new Headers({});

    return new Promise<[Mahasiswa]>(resolve => {
      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData.data);
          }
        });
    });
  }

  getAllListMahasiswa(type: number, session: any) {
    let responseData: any;
    let emailLecturer = session.email; // nanti ambil dari session
    let url = this.webService.url + "getAllListMhs.php";
    // Untuk dikirim ke API
    let requestData = {
      emailLecturer: emailLecturer,
      type: type
    };
    console.log(requestData);

    let header = new Headers({});

    return new Promise<[Mahasiswa]>(resolve => {
      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          console.log(response);
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData.data);
          }
        });
    });
  }

  insertBimbingan(student_id: any, type: number) {
    this.storage.get("session").then(val => {
      this.session = val;
    });

    let responseData: any;
    let lecturer_id = this.session.user_id; // nanti ambil dari session
    let url = this.webService.url + "insertBimbingan.php";
    // Untuk dikirim ke API
    let requestData = {
      lecturer_id: lecturer_id,
      student_id: student_id,
      type: type
    };
    console.log(requestData);

    let header = new Headers({});

    return new Promise<[Mahasiswa]>(resolve => {
      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          console.log(response);
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            console.log(responseData);
            resolve(responseData.success);
          }
        });
    });
  }

  getBankBimbingan(session: any) {
    let responseData: any;
    let lecturer_id = session.user_id; // nanti ambil dari session
    // let url = this.webService.url + "getBankBimbingan.php";
    let url = this.webService.url + "getBankBimbingan.php";
    // Untuk dikirim ke API
    let requestData = {
      lecturer_id: lecturer_id
    };
console.log(requestData);
    let header = new Headers({});

    return new Promise<[Task]>(resolve => {
      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          console.log(response);
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData.data);
          }
        });
    });
  }

  insertBimbinganCollection(description: string, weight: number, session: any, typeChoosen: number) {
    let responseData: any;
    let lecturer_id = session.user_id; // nanti ambil dari session
    let type = typeChoosen; // ambil dari session
    let url = this.webService.url + "insertBimbinganCollection.php";
    // Untuk dikirim ke API
    let requestData = {
      lecturer_id: lecturer_id,
      type: type,
      description: description,
      weight: weight
    };

    let header = new Headers({});

    return new Promise<[Mahasiswa]>(resolve => {
      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData);
          }
        });
    });
  }

    getSchedule(temp: any) {
    return new Promise<[Mahasiswa]>(resolve => {
      this.session = temp;
      // console.log(temp);
      let responseData: any;
      let lecturer_id = this.session.user_id; // ambil dari session
      let url = this.webService.url + "getSchedule.php";
      // Untuk dikirim ke API
      let requestData = {
        lecturer_id: lecturer_id
      };

      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {

          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData.data);
          }
        });
    });
  }

  getRequestBimbingan(data: any) {
    return new Promise(resolve => {
      // console.log(temp);
      let responseData: any;
      let lecturer_id = data.user_id; // ambil dari session
      let url = this.webService.url + "getRequestBimbingan.php";
      // Untuk dikirim ke API
      let requestData = {
        lecturer_id: lecturer_id
      };

      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData.data);
          }
        });
    });
  }

  setStatusSchedule(request: any, status: number) {
    return new Promise(resolve => {
      // console.log(temp);
      let responseData: any;
      let schedule_id = request.schedule_id; // ambil dari session
      let url = this.webService.url + "updateStatusSchedule.php";
      // Untuk dikirim ke API
      let requestData = {
        schedule_id: schedule_id,
        status: status
      };

      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData);
          }
        });
    });
  }

  updateBimbinganCollection(task: any) {
    return new Promise(resolve => {
      let responseData: any;
      let collection_id = task.collection_id;
      let url = this.webService.url + "updateBimbinganCollection.php";
      // Untuk dikirim ke API
      let requestData = {
        collection_id: collection_id,
        description: task.description,
        sort: task.sort,
        weight: task.weight
      };

      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData);
          }
        });
    });
  }

  getTodoListMahasiswa(mhs: any, session: any) {
    return new Promise(resolve => {
      let responseData: any;
      let student_id = mhs.user_id; // ambil dari session
      let url = this.webService.url + "getTodoListMahasiswa.php";
      // Untuk dikirim ke API
      let requestData = {
        student_id: student_id,
        lecturer_id: session.user_id
      };
      console.log(requestData);

      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          console.log(response);
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData.data);
          }
        });
    });
  }

  getUntukMahasiswa(mhs: any) {
    return new Promise(resolve => {
      let responseData: any;
      let student_id = mhs.user_id; // ambil dari session
      let url = this.webService.url + "getUntukProgress.php";
      // Untuk dikirim ke API
      let requestData = {
        student_id: student_id,
      };
      console.log(requestData);

      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          console.log(response);
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData.data);
          }
        });
    });
  }

  setStatusDone(task: any) {
    return new Promise(resolve => {
      let responseData: any;
      let bimbingan_detail_id = task.bimbingan_detail_id; // ambil dari session
      let status = task.status_done == 0 ? 1 : 0;
      let url = this.webService.url + "updateStatusBimbinganDetail.php";
      // Untuk dikirim ke API
      let requestData = {
        bimbingan_detail_id: bimbingan_detail_id,
        status: status
      };
      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData);
          }
        });
    });
  }

  deleteBimbinganDetail(task: any) {
    return new Promise(resolve => {
      let responseData: any;
      let bimbingan_detail_id = task.bimbingan_detail_id;
      let url = this.webService.url + "deleteBimbinganDetail.php";
      // Untuk dikirim ke API
      let requestData = {
        bimbingan_detail_id: bimbingan_detail_id
      };
      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          console.log(response);
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData);
          }
        });
    });
  }

  removeMahasiswa(mhs: any, session: any){
    return new Promise(resolve => {
      let responseData: any;
      let url = this.webService.url + "deleteMahasiswa.php";
      // Untuk dikirim ke API
      let requestData = {
        lecturer_id: session.user_id,
        student_id: mhs.user_id
      };
      console.log(requestData);
      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          console.log(response);
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData);
          }
        });
    });
  }

  insertBimbinganDetail(data: any, mhs: any) {
    return new Promise<[Mahasiswa]>(resolve => {
      console.log(mhs);
      console.log(data);
      let responseData: any;
      let url = this.webService.url + "insertBimbinganDetail.php";
      // Untuk dikirim ke API
      let requestData = {
        student_id: mhs.user_id,
        type: mhs.type,
        // bimbingan_detail_id: mhs.bimbingan_detail_id,
        description: data.desc,
        weight: data.weight
      };

      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          console.log(response);
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData);
          }
        });
    });
  }
}
