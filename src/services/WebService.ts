import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class WebService {
    url = 'http://misaelazarya.com/Mobile2/Project/';
    
    constructor(private http: Http) {
    }

    post(link : string, body : string, headers: Headers)
    {
        let _headers = headers;
        
        if(!headers){
            _headers = new Headers({ 'Content-Type': 'application/x-www-from-urlencoded' });
            _headers.append('Access-Control-Allow-Origin','*');
            _headers.append('Access-Control-Allow-Credentials','true');
            _headers.append('Access-Control-Allow-Headers: X-App-Token','Content-Type');
        }
        let options = new RequestOptions({ headers: _headers });
        return this.http.post(link, body, options);
    }

    get(link : string, headers: Headers){
        let _headers = headers;
        
        if(!headers){
            _headers = new Headers({ 'Content-Type': 'application/x-www-from-urlencoded' });
            _headers.append('Access-Control-Allow-Origin','*');
            _headers.append('Access-Control-Allow-Credentials','true');
            _headers.append('Access-Control-Allow-Headers: X-App-Token','Content-Type');
        }
        let options = new RequestOptions({ headers: _headers });
        return this.http.get(link, options);
    }

    put(link: string, headers: Headers, body: any, range: string){
        let options = new RequestOptions({ headers: headers });
        return this.http.put(link, body, options);
    }
}