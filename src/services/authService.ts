import { Injectable } from "@angular/core";
import { WebService } from "./WebService";
import { Headers } from "@angular/http";
import {
  AlertController,
  LoadingController,
  NavController
} from "ionic-angular";
import { LecturerMainPage } from "../pages/lecturer-main/lecturer-main";
import { StudentMainPage } from "../pages/student-main/student-main";
import { TabsPage } from "../pages/tabs/tabs";
import { TabsLecturerPage } from "../pages/tabs-lecturer/tabs-lecturer";
import { Storage } from "@ionic/storage";
import { Mahasiswa } from '../data/mahasiswa';
import swal from "sweetalert2";
import { Events } from 'ionic-angular';

@Injectable()
export class AuthService {
  loading: any;
  user: Mahasiswa;

  public user_id: string = "";
  public name: string = "";
  public email: string = "";
  public prodi: string = "";
  public nim: string = "";
  public angkatan: string = "";
  public position: string = "";
  public picture: string = "";

  constructor(
    public webService: WebService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public storage: Storage,
    public events: Events
  ) {}

  // getUser() {
  //   this.user = {
  //     user_id: this.user_id,
  //     email: this.email,
  //     name: this.name,
  //     nim: this.nim,
  //     prodi: this.prodi,
  //     angkatan: this.prodi,
  //     type: this.position,
  //   }
  //   return this.user;
  // }

  /** Login nembak ke webservice /login.php
   *
   * @param email string
   * @param password string
   * @param onSuccess Function
   */
  login(email: string, password: string, onSuccess: Function) {
    this.presentLoading();
    let url = this.webService.url + "login.php";

    // Untuk dikirim ke API
    let requestData = {
      email: email,
      password: password
    };

    let header = new Headers({});

    this.webService.post(url, JSON.stringify(requestData), header).subscribe(
      response => {
        if (response == null) {
        } else {
          let responseData: any = JSON.parse(response["_body"]);
          if (responseData.data[0] == null) {
            this.dismissLoading();
            // this.showAlert("Email or Password Wrong");
            swal("Email or Password Wrong", "", "error");
          } else {
            console.log(responseData.data[0]);
            // Jika 0 maka dosen
            if (responseData.data[0].position == 0) {
              onSuccess(LecturerMainPage);
            } else if (responseData.data[0].position == 1) {
              onSuccess(TabsPage);
            }

            // Set session use Ionic Storage
            this.user_id = responseData.data[0].user_id;
            this.name = responseData.data[0].name;
            this.email = responseData.data[0].email;
            this.nim = responseData.data[0].nim;
            this.angkatan = responseData.data[0].angkatan;
            this.position = responseData.data[0].position;
            this.prodi = responseData.data[0].prodi;
            this.picture = responseData.data[0].picture;

            const session = {
              user_id: this.user_id,
              name: this.name,
              email: this.email,
              prodi: this.prodi,
              nim: this.nim,
              angkatan: this.angkatan,
              position: this.position,
              picture: this.picture
            };
            this.storage.set("session", session);

            // // Or to get a key/value pair
            // this.storage.get("session").then(val => {
            //   console.log("Your age is", val.user_id);
            // });

            this.dismissLoading();

            // this.showAlert("Login Success");
            swal("Login Success", "", "success");
          }
        }
      },
      error => {
        this.presentAlert();
        this.dismissLoading();
      }
    );
  }

  logout(onSuccess: Function) {
    this.user_id = "";
    this.name = "";
    this.email = "";
    this.nim = "";
    this.angkatan = "";
    this.position = "";
    this.picture = "";

    const session = {
      user_id: this.user_id,
      name: this.name,
      email: this.email,
      prodi: this.prodi,
      nim: this.nim,
      angkatan: this.angkatan,
      position: this.position,
      picture: this.picture
    };
    this.storage.set("session", session);

    onSuccess();
  }

  signUpLecturer(
    email: string,
    name: string,
    password: string,
    picture: string
  ) {
    this.presentLoading();
    let url = this.webService.url + "signup.php";

    let requestData = {
      email: email,
      name: name,
      password: password,
      picture: picture
    };

    let header = new Headers({});

    this.webService.post(url, JSON.stringify(requestData), header).subscribe(
      response => {
        if (response == null) {
        } else {
          // console.log(response);
          let responseData: any = JSON.parse(response["_body"]);
          if (responseData == null) {
          } else {
            // console.log(responseData.success);
            // this.name = responseData.user.name;
            // this.id = responseData.user.id;
            // this.role = responseData.user.role;

            // const session = {
            //     name: this.name,
            //     id: this.id,
            //     role: this.role
            // }
            // this.storage.set('session', session);

            this.dismissLoading();

            // this.showAlert("Registartion Successful!");
            swal("Registration Successful", "", "success");
          }
        }
      },
      error => {
        this.presentAlert();
        this.dismissLoading();
      }
    );

    // this.dismissLoading();
  }

  signupStudent(
    email: string,
    name: string,
    nim: string,
    prodi: string,
    angkatan: string,
    password: string,
    picture: string
  ) {
    this.presentLoading();
    let url = this.webService.url + "signUpStudent.php";

    let requestData = {
      email: email,
      name: name,
      nim: nim,
      prodi: prodi,
      angkatan: angkatan,
      password: password,
      picture: picture
    };

    let header = new Headers({});

    this.webService.post(url, JSON.stringify(requestData), header).subscribe(
      response => {
        if (response == null) {
        } else {
          // console.log(response);
          let responseData: any = JSON.parse(response["_body"]);
          if (responseData == null) {
          } else {
            // console.log(responseData.success);
            // this.name = responseData.user.name;
            // this.id = responseData.user.id;
            // this.role = responseData.user.role;

            // const session = {
            //     name: this.name,
            //     id: this.id,
            //     role: this.role
            // }
            // this.storage.set('session', session);

            this.dismissLoading();

            // this.showAlert("Registartion Successful!");
            swal("Registration Successful", "", "success");
          }
        }
      },
      error => {
        this.presentAlert();
        this.dismissLoading();
      }
    );

    //this.dismissLoading();
  }

  presentAlert() {
    // this.showAlert("Login Failed");
    swal("Login Failed", "", "error");
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });

    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
    // this.loading.dismiss();
  }

  showAlert(title: string) {
    const alert = this.alertCtrl.create({
      title: title,
      buttons: [
        {
          text: "Ok",
          role: "cancel"
        }
      ]
    });

    alert.present();
  }

  editProfile(
    name: string,
    nim: string,
    prodi: string,
    angkatan: string,
    picture: string
  ) {
    return new Promise(resolve => {
      let responseData: any;
      let url = this.webService.url + "updateProfile.php";
      // Untuk dikirim ke API
      let requestData = {
        user_id: this.user_id,
        name: name,
        nim: nim,
        prodi: prodi,
        angkatan: angkatan,
        picture: picture
      };

      const session = {
        user_id: this.user_id,
        name: name,
        email: this.email,
        prodi: prodi,
        nim: nim,
        angkatan: angkatan,
        position: this.position,
        picture: picture
      };
      this.storage.set("session", session);

      this.name = name;
      this.nim = nim;
      this.angkatan = angkatan;
      this.prodi = prodi;
      this.picture = picture;

      this.events.publish('edit:success');

      // const session = {
      //   user_id: this.user_id,
      //   name: this.name,
      //   email: this.email,
      //   prodi: this.prodi,
      //   nim: this.nim,
      //   angkatan: this.angkatan,
      //   position: this.position,
      //   picture: this.picture
      // };
      // this.storage.set("session", session);

      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData["success"]);
          }
        });
    });
  }

  forgetpassword(email: string, randomNumber: any){
    return new Promise(resolve => {
      let responseData: any;
      let url = this.webService.url + "goSendEmail.php";
      // Untuk dikirim ke API
      let requestData = {
        email: email,
        randomNumber : randomNumber //untuk kirim random number code
      };
  
      let header = new Headers({});
      this.webService
      .post(url, JSON.stringify(requestData), header).subscribe(response => {
        if (response == null) {
          this.showAlert("Kesalahan Jaringan");
        } 
      });
        
    });
  }


  changePassword(email: string, password: string) {
    return new Promise(resolve => {
      let responseData: any;
      let url = this.webService.url + "changePassword.php";
      // Untuk dikirim ke API
      let requestData = {
        email: email,
        password: password
      };

      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData['success']);
            // this.showAlert("Password Has Been Changed!");
            swal("Password Has Been Changed ", "", "success");
          }
        });
    });
  }
}
