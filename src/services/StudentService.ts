import { Injectable } from "@angular/core";
import { WebService } from "./WebService";
import { Headers } from "@angular/http";
import { Storage } from "@ionic/storage";

@Injectable()
export class StudentService {
  constructor(private webService: WebService, private storage: Storage) {}

  getScheduleMahasiswa(userData: any) {
    return new Promise(resolve => {
      // console.log(temp);
      let responseData: any;
      let student_id = userData.user_id; // ambil dari session
      let url = this.webService.url + "getScheduleMahasiswa.php";
      // Untuk dikirim ke API
      let requestData = {
        student_id: student_id
      };

      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData.data);
          }
        });
    });
  }

  insertSchedule(userData: any, reqDate: any, type: any, status: any) {
    return new Promise(resolve => {
      // console.log(temp);
      let responseData: any;
      let student_id = userData.user_id; // ambil dari session
      let url = this.webService.url + "insertSchedule.php";
      // Untuk dikirim ke API
      let requestData = {
        student_id: student_id,
        reqDate: reqDate,
        type: type,
        status: status
      };

      let header = new Headers({});
      console.log(requestData);
      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          console.log(response);
          if (response == null) {
          } else {
            // responseData = JSON.parse(response["_body"]);
            resolve(true);
          }
        });
    });
  }

  getTodoList(userData: any, type: number) {
    console.log(userData);
    return new Promise(resolve => {
      // console.log(temp);
      let responseData: any;
      let student_id = userData.user_id; // ambil dari session
      let url = this.webService.url + "getTodoListMhs.php";
      // Untuk dikirim ke API
      let requestData = {
        student_id: student_id,
        type: type
      };
      console.log(requestData);

      let header = new Headers({});

      this.webService
        .post(url, JSON.stringify(requestData), header)
        .subscribe(response => {
          console.log(response);
          if (response == null) {
          } else {
            responseData = JSON.parse(response["_body"]);
            resolve(responseData.data);
          }
        });
    });
  }
}
