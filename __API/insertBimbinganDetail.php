<?php 
header('Access-Control-Allow-Origin: *');

	if (isset($_SERVER['HTTP_ORIGIN'])) {
	    header('Access-Control-Allow-Credentials: true');
	    header('Access-Control-Allow-Headers: X-App-Token, Content-Type');
   	}

	//ngambil data dari mobile
	$requestBody = json_decode(file_get_contents('php://input'), true);


	//set data yang uda diambil
	$student_id = $requestBody['student_id'];
	$type = $requestBody['type'];
	$desc = $requestBody['description'];
	$weight = $requestBody['weight'];

	//buka koneksi
	// $db = new mysqli('localhost', 'root', '', 'jb_online_umn'); //mysqli, pdo, mysql
	$db = new mysqli('localhost:3306', 'mise1565_admin', 'admin123', 'mise1565_jb_online_umn'); //mysqli, pdo, mysql

	// Ambil bimbingan_id
	$hasil = $db->query("SELECT bimbingan_id FROM `jb_bimbingan` WHERE `student_id` = $student_id AND `type` = $type");
	//ambil request_id
	$result = $hasil->fetch_assoc();
	$bimbingan_id = $result['bimbingan_id'];

	// ambil untuk sort
	$query ="SELECT COUNT(*)
			FROM jb_bimbingan_detail
			WHERE bimbingan_id = $bimbingan_id";
	$a = $db->query($query);
	$temp = mysqli_fetch_row($a);
	$jumlah = $temp[0]+1;

	// masukkan task ke bimbingan_detail
	$query = "INSERT INTO jb_bimbingan_detail (bimbingan_id, description, sort, weight, status_done) 
		VALUES ($bimbingan_id, '$desc', $jumlah, $weight, 0)";
	$data = $db->query($query);

	$return = null;
	if($data){
		$return['success'] = true;

		// get last row
		$query = "SELECT * 
				FROM jb_bimbingan_detail
				ORDER BY 1 DESC
				LIMIT 1";
		$result = $db->query($query);
		$row = mysqli_fetch_all($result,MYSQLI_ASSOC);
		$return["data"] = $row;
	}
	else{
		$return['success'] = false;
	}

	echo json_encode($return);

?>