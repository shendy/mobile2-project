-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 29, 2018 at 04:22 AM
-- Server version: 5.7.13-log
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jb_online_umn`
--

-- --------------------------------------------------------

--
-- Table structure for table `jb_bimbingan`
--

CREATE TABLE `jb_bimbingan` (
  `bimbingan_id` int(11) NOT NULL,
  `lecturer_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jb_bimbingan_collection`
--

CREATE TABLE `jb_bimbingan_collection` (
  `collection_id` int(11) NOT NULL,
  `lecturer_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jb_bimbingan_detail`
--

CREATE TABLE `jb_bimbingan_detail` (
  `bimbingan_id` int(11) NOT NULL,
  `bimbingan_ke` int(11) NOT NULL DEFAULT '0',
  `status_done` int(11) DEFAULT '0',
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jb_schedule`
--

CREATE TABLE `jb_schedule` (
  `schedule_id` int(11) NOT NULL,
  `lecturer_id` int(11) NOT NULL,
  `student_id` int(11) DEFAULT NULL,
  `start_time` date NOT NULL,
  `end_time` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jb_user`
--

CREATE TABLE `jb_user` (
  `user_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `name` varchar(128) NOT NULL,
  `nim` varchar(15) DEFAULT NULL,
  `position` int(11) NOT NULL COMMENT '0=dosen, 1=mahasiswa'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jb_user`
--

INSERT INTO `jb_user` (`user_id`, `email`, `password`, `name`, `nim`, `position`) VALUES
(1, 'dosen', 'asd', 'namadosen', '123', 0),
(2, 'mahasiswa', 'asd', 'namamahasiswa', '123', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jb_bimbingan`
--
ALTER TABLE `jb_bimbingan`
  ADD PRIMARY KEY (`bimbingan_id`),
  ADD KEY `lecturer_id` (`lecturer_id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `jb_bimbingan_collection`
--
ALTER TABLE `jb_bimbingan_collection`
  ADD PRIMARY KEY (`collection_id`),
  ADD KEY `lecturer_id` (`lecturer_id`);

--
-- Indexes for table `jb_bimbingan_detail`
--
ALTER TABLE `jb_bimbingan_detail`
  ADD PRIMARY KEY (`bimbingan_id`);

--
-- Indexes for table `jb_schedule`
--
ALTER TABLE `jb_schedule`
  ADD PRIMARY KEY (`schedule_id`),
  ADD KEY `lecturer_id` (`lecturer_id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `jb_user`
--
ALTER TABLE `jb_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jb_bimbingan`
--
ALTER TABLE `jb_bimbingan`
  MODIFY `bimbingan_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jb_bimbingan_collection`
--
ALTER TABLE `jb_bimbingan_collection`
  MODIFY `collection_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jb_bimbingan_detail`
--
ALTER TABLE `jb_bimbingan_detail`
  MODIFY `bimbingan_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jb_schedule`
--
ALTER TABLE `jb_schedule`
  MODIFY `schedule_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jb_user`
--
ALTER TABLE `jb_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `jb_bimbingan`
--
ALTER TABLE `jb_bimbingan`
  ADD CONSTRAINT `jb_bimbingan_ibfk_1` FOREIGN KEY (`lecturer_id`) REFERENCES `jb_user` (`user_id`),
  ADD CONSTRAINT `jb_bimbingan_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `jb_user` (`user_id`);

--
-- Constraints for table `jb_bimbingan_collection`
--
ALTER TABLE `jb_bimbingan_collection`
  ADD CONSTRAINT `jb_bimbingan_collection_ibfk_1` FOREIGN KEY (`lecturer_id`) REFERENCES `jb_user` (`user_id`);

--
-- Constraints for table `jb_bimbingan_detail`
--
ALTER TABLE `jb_bimbingan_detail`
  ADD CONSTRAINT `jb_bimbingan_detail_ibfk_1` FOREIGN KEY (`bimbingan_id`) REFERENCES `jb_bimbingan` (`bimbingan_id`);

--
-- Constraints for table `jb_schedule`
--
ALTER TABLE `jb_schedule`
  ADD CONSTRAINT `jb_schedule_ibfk_1` FOREIGN KEY (`lecturer_id`) REFERENCES `jb_user` (`user_id`),
  ADD CONSTRAINT `jb_schedule_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `jb_user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
