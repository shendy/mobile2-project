-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 12, 2018 at 07:26 AM
-- Server version: 5.7.13-log
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jb_online_umn`
--

-- --------------------------------------------------------

--
-- Table structure for table `jb_bimbingan`
--

CREATE TABLE `jb_bimbingan` (
  `bimbingan_id` int(11) NOT NULL,
  `lecturer_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '0=magang, 1=skripsi'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jb_bimbingan`
--

INSERT INTO `jb_bimbingan` (`bimbingan_id`, `lecturer_id`, `student_id`, `type`) VALUES
(1, 1, 2, 0),
(2, 1, 4, 0),
(3, 1, 2, 1),
(5, 6, 4, 0),
(12, 1, 3, 1),
(16, 1, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jb_bimbingan_collection`
--

CREATE TABLE `jb_bimbingan_collection` (
  `collection_id` int(11) NOT NULL,
  `lecturer_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jb_bimbingan_collection`
--

INSERT INTO `jb_bimbingan_collection` (`collection_id`, `lecturer_id`, `type`, `description`, `sort`, `weight`) VALUES
(1, 1, 0, 'task1', 1, 10),
(2, 1, 0, 'task2', 2, 20),
(4, 1, 0, 'task4', 3, 30),
(17, 1, 0, 'task5', 4, 40),
(18, 1, 0, 'task baru', 5, 50),
(20, 1, 0, 'dasd', 6, 15),
(21, 1, 0, '', 7, 12);

-- --------------------------------------------------------

--
-- Table structure for table `jb_bimbingan_detail`
--

CREATE TABLE `jb_bimbingan_detail` (
  `bimbingan_detail_id` int(11) NOT NULL,
  `bimbingan_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `weight` int(11) DEFAULT '0',
  `status_done` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jb_bimbingan_detail`
--

INSERT INTO `jb_bimbingan_detail` (`bimbingan_detail_id`, `bimbingan_id`, `description`, `sort`, `weight`, `status_done`) VALUES
(1, 16, 'task1', 1, 10, 0),
(2, 16, 'task2', 2, 20, 0),
(3, 16, 'task4', 3, 30, 1),
(5, 16, 'task baru', 5, 50, 1),
(7, 12, 'asdasd', 1, 0, 0),
(9, 16, 'tampan', 6, 123, 0),
(10, 16, 'max', 6, 450, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jb_schedule`
--

CREATE TABLE `jb_schedule` (
  `schedule_id` int(11) NOT NULL,
  `lecturer_id` int(11) NOT NULL,
  `student_id` int(11) DEFAULT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) DEFAULT '0' COMMENT '0=request 1=diterima 2=ditolak',
  `type` int(11) NOT NULL COMMENT '0=magang, 1=skripsi'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jb_schedule`
--

INSERT INTO `jb_schedule` (`schedule_id`, `lecturer_id`, `student_id`, `start_time`, `end_time`, `status`, `type`) VALUES
(1, 1, 2, '2018-12-09 11:00:00', '2018-12-09 12:00:00', 1, 1),
(2, 1, 3, '2018-12-10 11:00:00', '2018-12-10 12:00:00', 1, 0),
(3, 1, 3, '2018-12-09 08:00:00', '2018-12-09 09:00:00', 0, 1),
(16, 1, 3, '2018-12-11 07:56:00', '2018-12-11 09:56:00', 0, 0),
(17, 1, 3, '2018-12-11 13:00:00', '2018-12-11 15:00:00', 0, 0),
(18, 1, 4, '2018-12-10 11:01:00', '2018-12-10 13:01:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jb_user`
--

CREATE TABLE `jb_user` (
  `user_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `name` varchar(128) NOT NULL,
  `nim` varchar(15) DEFAULT NULL,
  `prodi` varchar(255) DEFAULT NULL,
  `angkatan` varchar(255) DEFAULT NULL,
  `position` int(11) NOT NULL COMMENT '0=dosen, 1=mahasiswa'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jb_user`
--

INSERT INTO `jb_user` (`user_id`, `email`, `password`, `name`, `nim`, `prodi`, `angkatan`, `position`) VALUES
(1, 'dosen', 'asd', 'namadosen', '', '', '', 0),
(2, 'shendyharlim', 'asd', 'sprol', '1234', 'Ilkom', '2017', 1),
(3, 'shendy', 'ad', 'shendy', '123', 'asd', 'asd', 1),
(4, 'mahasiswa1', 'asd', 'namaMahasiswa1', '123', 'Informatika', '2016', 1),
(5, 'mahasiswa2', 'asd', 'namaMahasiswa2', '345', 'Informatika', '2015', 1),
(6, 'dosen2', 'asd', 'NamaDosen2', NULL, NULL, NULL, 0),
(7, '', '', '', NULL, NULL, NULL, 0),
(8, '', '', '', '', '', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jb_bimbingan`
--
ALTER TABLE `jb_bimbingan`
  ADD PRIMARY KEY (`bimbingan_id`),
  ADD KEY `lecturer_id` (`lecturer_id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `jb_bimbingan_collection`
--
ALTER TABLE `jb_bimbingan_collection`
  ADD PRIMARY KEY (`collection_id`),
  ADD KEY `lecturer_id` (`lecturer_id`);

--
-- Indexes for table `jb_bimbingan_detail`
--
ALTER TABLE `jb_bimbingan_detail`
  ADD PRIMARY KEY (`bimbingan_detail_id`),
  ADD KEY `bimbingan_id` (`bimbingan_id`);

--
-- Indexes for table `jb_schedule`
--
ALTER TABLE `jb_schedule`
  ADD PRIMARY KEY (`schedule_id`),
  ADD KEY `lecturer_id` (`lecturer_id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `jb_user`
--
ALTER TABLE `jb_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jb_bimbingan`
--
ALTER TABLE `jb_bimbingan`
  MODIFY `bimbingan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `jb_bimbingan_collection`
--
ALTER TABLE `jb_bimbingan_collection`
  MODIFY `collection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `jb_bimbingan_detail`
--
ALTER TABLE `jb_bimbingan_detail`
  MODIFY `bimbingan_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `jb_schedule`
--
ALTER TABLE `jb_schedule`
  MODIFY `schedule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `jb_user`
--
ALTER TABLE `jb_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `jb_bimbingan`
--
ALTER TABLE `jb_bimbingan`
  ADD CONSTRAINT `jb_bimbingan_ibfk_1` FOREIGN KEY (`lecturer_id`) REFERENCES `jb_user` (`user_id`),
  ADD CONSTRAINT `jb_bimbingan_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `jb_user` (`user_id`);

--
-- Constraints for table `jb_bimbingan_collection`
--
ALTER TABLE `jb_bimbingan_collection`
  ADD CONSTRAINT `jb_bimbingan_collection_ibfk_1` FOREIGN KEY (`lecturer_id`) REFERENCES `jb_user` (`user_id`);

--
-- Constraints for table `jb_bimbingan_detail`
--
ALTER TABLE `jb_bimbingan_detail`
  ADD CONSTRAINT `jb_bimbingan_detail_ibfk_1` FOREIGN KEY (`bimbingan_id`) REFERENCES `jb_bimbingan` (`bimbingan_id`);

--
-- Constraints for table `jb_schedule`
--
ALTER TABLE `jb_schedule`
  ADD CONSTRAINT `jb_schedule_ibfk_1` FOREIGN KEY (`lecturer_id`) REFERENCES `jb_user` (`user_id`),
  ADD CONSTRAINT `jb_schedule_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `jb_user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
