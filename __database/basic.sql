DROP DATABASE IF EXISTS jb_online_umn;
CREATE DATABASE jb_online_umn;
USE jb_online_umn;

CREATE TABLE jb_user(
	user_id		INT PRIMARY KEY AUTO_INCREMENT,
	email 		VARCHAR(50) NOT NULL,
	password 	VARCHAR(64) NOT NULL,
	first_name 	VARCHAR(128) NOT NULL,
	last_name 	VARCHAR(128),
	nim			VARCHAR(15) NOT NULL,
	position 	INT NOT NULL
) ENGINE=INNODB;

CREATE TABLE jb_schedule(
	schedule_id		INT PRIMARY KEY AUTO_INCREMENT,
	lecturer_id 	INT NOT NULL,
	student_id		INT,
	start_time		DATE NOT NULL,
	end_time		DATE NOT NULL,
	status			INT NOT NULL,
	FOREIGN KEY (lecturer_id) REFERENCES jb_user(user_id),
	FOREIGN KEY (student_id) REFERENCES jb_user(user_id)
) ENGINE=INNODB;

CREATE TABLE jb_bimbingan(
	bimbingan_id 	INT PRIMARY KEY AUTO_INCREMENT,
	lecturer_id		INT NOT NULL,
	student_id		INT NOT NULL,
	type			INT NOT NULL,
	FOREIGN KEY (lecturer_id) REFERENCES jb_user(user_id),
	FOREIGN KEY (student_id) REFERENCES jb_user(user_id)
) ENGINE=INNODB;

CREATE TABLE jb_bimbingan_detail(
	bimbingan_id	INT PRIMARY KEY AUTO_INCREMENT,
	bimbingan_ke	INT DEFAULT 0 NOT NULL,
	status_done		INT DEFAULT 0,
	description		VARCHAR(255),
	FOREIGN KEY (bimbingan_id) REFERENCES jb_bimbingan(bimbingan_id)
) ENGINE=INNODB;

CREATE TABLE jb_bimbingan_collection(
	collection_id	INT PRIMARY KEY AUTO_INCREMENT,
	lecturer_id		INT NOT NULL,
	type			INT NOT NULL,
	description		VARCHAR(255),
	FOREIGN KEY (lecturer_id) REFERENCES jb_user(user_id)
) ENGINE=INNODB;